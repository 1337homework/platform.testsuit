const colors = require('colors/safe')
const request = require('request')
const Q = require('q')

const reportFailure = (msg, err) => {
  console.error(colors.red(msg), err)
  process.exit(1)
}

const reportSuccess = (msg) => {
  console.log(colors.green(msg))
}

const callRemote = (reqOptions) => {
  const deferred = Q.defer()

  request(reqOptions, (err, httpResponse, body) => {
    if (err) {
      deferred.reject(new Error(err))
    } else {
      deferred.resolve({ ...httpResponse, ...body })
    }
  })

  return deferred.promise
}

module.exports = {
  reportFailure,
  reportSuccess,
  callRemote
}
