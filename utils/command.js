const {
  reportFailure,
  reportSuccess,
  callRemote
} = require('./helper')
const config = require('../configuration/config')

const httpToAmqpReq = (method, path = '', reqBody = {}, headers = {}, cb, positive = true) => {
  const positiveness = positive ? 'POSITIVE' : 'NEGATIVE'
  const url = config.endpoint + path

  return callRemote({
    url,
    body: JSON.stringify(reqBody),
    headers: Object.assign({}, config.headers, headers),
    method
  })
    .then((response) => cb(response))
    .finally(() => reportSuccess(` (X) httpToAmqpReq - ${positiveness} - ${method.toUpperCase()} - ${url} test passed.`))
    .catch((err) => reportFailure(` (X) httpToAmqpReq - ${positiveness} - ${method.toUpperCase()} - ${url} test failed.`, err))
}

module.exports = {
  httpToAmqpReq
}
