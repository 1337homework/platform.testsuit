const WebSocket = require('ws')

const ws1 = new WebSocket('ws://localhost:8886')

ws1.onopen = () => {
  console.log('Connected to Aggregator Server')
}

ws1.onmessage = (message) => {
  console.log(message.data)
}
