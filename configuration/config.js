module.exports = {
  endpoint: process.env.APIPROXY_ENDPOINT || 'http://localhost:8883',
  headers: {
    'User-Agent': process.env.USER_AGENT || 'request',
    'Content-Type': process.env.CONTENT_TYPE || 'application/json',
    'Origin': process.env.ORIGIN || 'http://localhost',
    'Access-Control-Request-Headers': process.env.ACCESS_CONTROL_REQUEST_HEADERS || '',
  },
  login: {
    path:'/api/login'
  },
  saga: {
    path:'/api/saga'
  }
}
