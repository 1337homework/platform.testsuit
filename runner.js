require('dotenv').config()
const assert = require('assert')
const jwt = require('jsonwebtoken')
const config = require('./configuration/config')
const { entity, command } = require('./utils')

command.httpToAmqpReq('post', config.login.path, {}, {}, (response) => {
  assert.equal(response.statusCode, '400')
  assert.ok(response.body)
}, false)
command.httpToAmqpReq('post', config.login.path, entity.loginData(), {}, (response) => {
  assert.equal(response.statusCode, '200')
  assert.ok(response.body)
})
command.httpToAmqpReq('post', config.login.path, entity.loginData(), {}, (response) => {
  const parsed = JSON.parse(response.body)
  assert.equal(parsed.authStatus, 'OK')
  assert.doesNotThrow(() => jwt.decode(parsed.authToken))
})
command.httpToAmqpReq('post', config.login.path, entity.loginData(), {}, async (response) => {
  assert.ok(response)
  let loggedInUser = { ...response.body }
  let result = await command.httpToAmqpReq('post', config.login.path, {}, loggedInUser, (res) => res)
  assert.equal(result.statusCode, '400')
}, false)
// command.httpToAmqpReq('post', config.login.path, entity.loginData(), {}, async (response) => {
//   assert.ok(response)
//   let malformedTokenBody = Object.assign(response.body, { authToken: 'malformed token' })
//   let result = await command.httpToAmqpReq('post', config.login.path, {}, malformedTokenBody, (res) => res)
//   console.log(result)
// })
